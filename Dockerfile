FROM python:3.8
WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apt-get update -y

RUN apt-get install -y libpq-dev

RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY . .